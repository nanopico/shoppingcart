<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ListingController@index')->name('listing');
Route::post('/', 'ListingController@post')->name('listingPost');

Route::get('/cart', 'CartController@index')->name('cart.listing');
Route::post('/delete', 'CartController@deleteProduct')->name('cart.delete');
Route::post('/update', 'CartController@updateProduct')->name('cart.update');
Route::get('/get', 'CartController@getProducts')->name('cart.get');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');