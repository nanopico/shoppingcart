<?php

namespace App\Http\Controllers;


use App\Models\UserCart;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Cart
{
    protected $cart = [];

    /**
     * Cart constructor.
     */
    public function __construct()
    {
        $user = request()->user();

        if(is_null($user)){
            $cart = Session::get('cart');
            $this->cart = (array)json_decode($cart);
        }else{
            $userCart = UserCart::where('user_id', $user->id)->first();
            if(!is_null($userCart)){
                $this->cart = (array)json_decode($userCart->cart);
            }else{
                $this->cart = [];
            }
        }
    }

    /**
     * @param null $productId
     * @param int $quantity
     * @return \Illuminate\Http\JsonResponse
     */
    public function add($productId = null, $quantity = 1)
    {
        $cart = json_decode(json_encode($this->cart), true);
        $cart[(string)$productId]= [
            'quantity' => $quantity,
        ];
        $this->cart = $cart;
        $this->saveCart();
        return $this->returnResponse($this->get());
    }

    /**
     * @param $productId
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove($productId)
    {
        $cart = json_decode(json_encode($this->cart));
        unset($cart->$productId);
        $this->cart = (array)$cart;
        $this->saveCart();
        return $this->returnResponse(['success' => true]);
    }

    /**
     * @param $productId
     * @param null $quantity
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($productId, $quantity = null)
    {
        if (!is_null($quantity)) {
            $this->cart[$productId]['quantity'] = $quantity;
        }
        $this->saveCart();
        return $this->returnResponse(['success' => true]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function get()
    {
        return $this->returnResponse($this->cart);
    }

    /**
     * @return int
     */
    public function count(){
        return count($this->cart);
    }

    /**
     *
     */
    protected function saveCart()
    {
        $userId = Auth::id();
        if(is_null($userId)){
            Session::put('cart', json_encode($this->cart));
        }else{
            $userCart = UserCart::where('user_id', $userId)->first();
            if(is_null($userCart)){
                $userCart = new UserCart();
                $userCart->user_id = $userId;
                $userCart->cart = json_encode($this->cart);
                $userCart->save();
            }else{
                $userCart->cart = json_encode($this->cart);
                $userCart->save();
            }
        }
    }

    /**
     * @param $response
     * @return \Illuminate\Http\JsonResponse
     */
    private function returnResponse($response){
        if(request()->headers->get('Content-Type') == "application/json"){
            return response()->json($response);
        }
        return $response;
    }
}
