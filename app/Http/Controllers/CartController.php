<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * @param Cart $cart
     * @return $this
     */
    public function index(Cart $cart){
        $cartProducts = $cart->get();
        foreach ($cartProducts as $key => $cartProduct){
            $cartProduct->object = Product::find($key);
        }

        return view('cart')->with(compact('cartProducts'));
    }

    /**
     *
     */
    public function createOrder(){
    }

    /**
     * @param Cart $cart
     * @return int
     */
    public function deleteProduct(Cart $cart){
        $cart->remove(\request()->get('id'));

        return $cart->count();
    }

    /**
     * @param Cart $cart
     * @return int
     */
    public function updateProduct(Cart $cart){
        $cart->update(request()->get('id'), request()->get('quantity'));
        return $cart->count();
    }

    /**
     * @param Cart $cart
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProducts(Cart $cart){
        $cartProducts = $cart->get();
        foreach ($cartProducts as $key => $cartProduct){
            $cartProduct->object = Product::find($key);
        }
        return $cartProducts;
    }
}
