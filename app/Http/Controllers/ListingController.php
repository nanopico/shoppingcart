<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Input;
use Illuminate\Cookie\CookieJar;

class ListingController extends Controller
{
    /**
     * @param Cart $cart
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Cart $cart){
        $cartProducts = $cart->get();
        $products = Product::all();
        return view('listing', compact('products', 'cartProducts'));
    }

    /**
     * @param Cart $cart
     * @return array
     */
    public function post(Cart $cart){
        request()->validate([
            'productId' => 'required'
        ]);
        if(request()->has('productId')){
            $cart->add(request()->get('productId'), request()->get('productQte', 1) );
        }
        return ['cartProducts' => $cart->count()];
    }
}
