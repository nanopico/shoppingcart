<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserCart extends Model
{
    public $timestamps = true;
    protected $table = 'users_carts';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
