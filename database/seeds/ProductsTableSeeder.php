<?php

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 20; $i++) {
            Product::create([
                'name' => $faker->name,
                'description' => $faker->sentence,
                'image_url' => 'https://www.random.org/bitmaps/?format=png&width=128&height=128&zoom=1',
                'price' => $faker->randomFloat(3, 1, 100),
            ]);
        }
    }
}
