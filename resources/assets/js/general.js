$(document).ready(function () {
    var totalPrice = 0;
    $('img[data-image]').each(function () {
        var src = $(this).data("image");
        $(this).attr("src", src);
    });

    $('[data-delete-product]').click(function () {
        var id = $(this).data('delete-product');
        var url = $('[data-delete-product-url]').data('delete-product-url');
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            method: "POST",
            url: url,
            data: {'id':id, '_token': CSRF_TOKEN},
            dataType: 'JSON',
            success: (function( response ) {
                $('[data-product-'+id+'-cart]').remove();
                $('[data-cart-products-counter] span').html(response);
                calculateTotal();
            })
        });
        $('form[name="'+$(this).data('delete-product')+'"]').submit();
    });

    function calculateTotal(){
        totalPrice = 0;
        $('[data-calculate-cart]').each(function () {
            var productId = $(this).data('delete-product');
            var quantity =$('[data-quantity-'+productId+']').val();
            var price = $('[data-quantity-'+productId+']').data('price');
            totalPrice += quantity*price;
        });
        $('body [data-total]').html(parseFloat(totalPrice).toFixed(2));
    }

    $( "input[data-price]" ).change(function() {
        var id = $(this).data('update-product-id');
        var quantity = $(this).val();
        var url = $(this).data('update-url');
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            method: "POST",
            url: url,
            data: {'id':id, 'quantity': quantity, '_token': CSRF_TOKEN},
            dataType: 'JSON',
            success: (function( response ) {
                calculateTotal();
                $('[data-quantity-product-'+id+']').html(quantity);
            })
        });
        calculateTotal();
    });

    calculateTotal();
});