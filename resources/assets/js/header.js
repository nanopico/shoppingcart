$('document').ready(function(){
    var totalPrice = 0;
    $('[data-cart-products-counter]').html(function () {
        return "<span style='padding-left: 10px'>"+$(this).data('cart-products-counter')+"</span>";
    });
    $('.cart').click(function () {
        $('.cart-items').toggle();
        cartItems();
    });

    function cartItems() {
        var cartProductsUrl = $('[data-cart-products-url]').data('cart-products-url');
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            method: "GET",
            url: cartProductsUrl,
            data: {'_token': CSRF_TOKEN},
            dataType: 'JSON',
            success: (function( response ) {
                var cartItems = '';
                $.each(response, function (key, productObj) {
                    cartItems += createCartProductHtml(key, productObj)
                });
                $('[data-content]').html(cartItems);
                calculateTotal();
            })
        });
    }

    function createCartProductHtml(productId, productObj) {
        return '<div class="card" style="width: 35rem;">'+
                    '<div class="card-body">'+
                        '<h4 class="card-title">'+productObj.object.name+'</h4>'+
                        '<div class="row">'+
                            '<div class="col-xs-12 text-left">'+
                                'Price: '+productObj.object.price+' &euro;'+
                            '</div>'+
                            '<div class="col-xs-12 text-left">'+
                                '<span class="pull-left">Quantity: <b><span data-cart-item-id="'+productObj.object.id+'" data-cart-item-price='+productObj.object.price+' data-quantity-product-'+productId+'="'+productObj.quantity+'">'+productObj.quantity+'</span></b></span>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>';
    }

    function calculateTotal(){
        totalPrice = 0;
        $('body [data-cart-item-price]').each(function () {
            var productId = $(this).data('cart-item-id');
            var quantity =$(this).data('quantity-product-'+productId);
            var price = $(this).data('cart-item-price');
            totalPrice += quantity*price;
        });
        $('[data-total]').html(parseFloat(totalPrice).toFixed(2));
    }

    cartItems();

    $('form[name="addProduct"]').submit(function (e) {
        e.preventDefault();
        var $this = $(this);
        var addCartUrl = $this.find('[data-add-cart]').data('add-cart');
        var formData = $this.serialize();
        $.ajax({
            method: "POST",
            url: addCartUrl,
            data: formData,
            success: (function( response ) {
                console.log(response.cartProducts);
                $('[data-cart-products-counter] span').html(response.cartProducts);
                cartItems();
            })
        });
    });

    // $('.cart').blur(function () {
    //     $('.cart-items').hide();
    // });
});
