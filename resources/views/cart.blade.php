@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="navbar navbar-default navbar-static-top"> Total: <span data-total></span></div>
        <form action="{{route('listingPost')}}" name="addProduct" method="post">
            @foreach($cartProducts as $cartProduct)
                @include('componentsCartListing.product')
            @endforeach
        </form>

    </div>
@endsection
@push('scripts')
    <script>

    </script>
@endpush