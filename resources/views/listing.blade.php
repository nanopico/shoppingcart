@extends('layouts.app')

@section('content')
    <div class="container">
        @foreach($products as $key=>$product)
            @include('componentsListing.product')
        @endforeach
    </div>
@endsection
@push('scripts')
    <script>

    </script>
@endpush