<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <div data-delete-product-url="{{route('cart.delete')}}" hidden></div>
    <div data-cart-products-url="{{route('cart.get')}}" hidden></div>
    <nav class="navbar navbar-default navbar-static-top" style="    z-index: 99999;">
        <div class="container">
            <div class="navbar-header">
                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>
            <!-- Right Side Of Navbar -->
            <div class="pull-left">
                <ul class="nav navbar-nav navbar-right" @guest style="flex-direction: row;" @endguest>
                    <!-- Authentication Links -->
                    @guest
                        <li><a href="{{ route('login') }}" >Login</a></li>
                        <li><a href="{{ route('register') }}" >Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false">
                                {{ Auth::user()->name }}
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endguest
                </ul>
                @if(isset($cartProducts))
                    <div class="cart pull-right nav navbar-nav navbar-right" ><i class="glyphicon glyphicon-shopping-cart" data-cart-products-counter="{{count($cartProducts)}}"></i></div>
                    <div class="cart-items" style="position: absolute; display: none; top:60px;">
                        <div class="card" style="width: 35rem; text-align: center;"><u>Total:</u> <span data-total></span></div>
                        <div data-content></div>
                        <div class="card" style="width: 35rem; text-align: center;"><a href="{{route('cart.listing')}}">Cart</a></div>
                    </div>
                @endif
            </div>
        </div>
    </nav>

    @yield('content')
</div>

<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="{{ asset('js/app.js') }}"></script>
@stack('scripts')
</body>
</html>
