<div class="card" data-product-{{$cartProduct->object->id}}-cart style="margin-bottom: 20px;">
    <div class="card-body">
        <div class="row">
            <div class="col-xs-12">
                <div data-delete-product="{{$cartProduct->object->id}}" data-calculate-cart> <i class="glyphicon glyphicon-trash pull-right"></i> </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-2">
                <img class="card-img-left" src="#" alt="Card image cap" data-image="{{$cartProduct->object->image_url}}">
            </div>
            <div class="col-xs-10">
                <h4 class="card-title">{{$cartProduct->object->name}}</h4>
                <p class="card-text">{{$cartProduct->object->description}}</p>
                <div class="row">
                    <div class="col-xs-12 text-left">
                        Price: {{number_format($cartProduct->object->price, 2)}} &euro;
                    </div>
                    <div class="col-xs-12 text-left">
                        {!! csrf_field() !!}
                        Quantity: <input type="number" data-update-url="{{route('cart.update')}}" data-quantity-{{$cartProduct->object->id}} data-update-product-id={{$cartProduct->object->id}} data-price="{{number_format($cartProduct->object->price, 2)}}" name="productQte[{{$cartProduct->object->id}}]['quantity']" value="{{$cartProduct->quantity}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
