@if($key%2 == 0)
    <div class="row">
        @endif
        <div class="col-md-6">
            <div class="card">
                <img class="card-img-top" src="#" alt="Card image cap" data-image="{{$product->image_url}}">
                <div class="card-body">
                    <h4 class="card-title">{{$product->name}}</h4>
                    <p class="card-text">{{$product->description}}</p>
                    <div class="row" style="border-top: 1px solid #ccd0d2;padding: 10px 0 0;">
                        <div class="col-xs-6 text-center">
                            {{number_format($product->price, 2) }} &euro;
                        </div>
                        <div class="col-xs-6 text-center">
                            <form action="{{route('listingPost')}}" name="addProduct" method="post">
                                {!! csrf_field() !!}
                                <input type="hidden" name="productId" value="{{$product->id}}">
                                <input type="hidden" name="productQte" value="1">
                                <input type="submit" class="btn btn-primary" value="Add to cart" data-add-cart="#">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if($key%2 == 1)
            <div class="clearfix"></div>
    </div>
    <br>
@endif
